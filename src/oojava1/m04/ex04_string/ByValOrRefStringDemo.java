package oojava1.m04.ex04_string;

public class ByValOrRefStringDemo {

	public static void main(String[] args) {
		String a = "A-String";
		System.out.printf("Value of variable                                : %s\n", a);
		changeValue(a);
		System.out.printf("Value of variable outside method after increment : %s\n", a);
	}
	
	public static void changeValue(String x) {
		x=x+" CHANGED";
		System.out.printf("Value of variable inside method after increment  : %s\n", x);
	}

}
