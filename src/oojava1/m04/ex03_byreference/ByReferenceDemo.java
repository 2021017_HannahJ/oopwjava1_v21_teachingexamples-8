package oojava1.m04.ex03_byreference;

public class ByReferenceDemo {

	public static void main(String[] args) {
		IntContainer n = new IntContainer(10);
		System.out.printf("Value of variable                                : %d\n", n.getInt());
		incValue(n);
		System.out.printf("Value of variable outside method after increment : %d\n", n.getInt());
	}
	
	public static void incValue(IntContainer m) {
		m.inc();
		System.out.printf("Value of variable inside method after increment  : %s\n", ""+m); // notice the use of toString().
	}
}


class IntContainer {
	private int n;
	public IntContainer(int n) {
		this.n = n;
	}
	public int getInt() { return n;	}
	public void setInt(int n) {this.n = n;}
	public void inc() {n++;}
	public String toString() {return Integer.toString(n);}
}